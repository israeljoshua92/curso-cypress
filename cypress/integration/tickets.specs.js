describe("Tickets", () =>{
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

    it("fills all the text input text", () => {
        const firstName = "Israel"
        const lastName = "Amoêdo"

        cy
            .get("#first-name").type(firstName)
            .get("#last-name").type(lastName)
            .get("#email").type("test@gmail.com")
            .get("#requests").type("Vegetarian")
            .get("#signature").type(`${firstName} ${lastName}`)
    })

    it("select 2 tickets", () => {
        cy
            .get("#ticket-quantity").select("2")
    })

    it("select 'vip' ticket type", () => {
        cy
            .get("#vip").check()
    })

    it("selects 'social media' and 'friend' check boxes", () =>{
        cy
            .get("#friend").check()
            .get("#social-media").check()
    })

    it("select 'friend' and 'publication' and then uncheck 'friend'", () => {
        cy
            .get("#friend").check()
            .get("#publication").check()
            .get("#friend").uncheck()
    })

    it("has 'TICKETBOX' hearder's heading", () =>{
        cy
            .get("header h1").should("contain", "TICKETBOX")
    })

    it("alerts on invalid email", () => {
        cy.get("#email")
          .as("email")
          .type("talkingabouttesting-gmail.com")
            
        cy.get("#email.invalid").should("exist")
        
        cy.get("@email")
          .clear()
          .type("talkingabouttesting@gmail.com")
        
        cy.get("#email.invalid").should("not.exist")
    })

    it("fills and reset the form", () => {
        const firstName = "Israel"
        const lastName = "Amoêdo"
        const fullName = `${firstName} ${lastName}`

        cy
            .get("#first-name").type(firstName)
            .get("#last-name").type(lastName)
            .get("#email").type("test@gmail.com")
            .get("#ticket-quantity").select("2")
            .get("#vip").check()
            .get("#friend").check()
            .get("#requests").type("IPA beer")
        cy 
            .get(".agreement p").should(
                "contain",
                `I, ${fullName}, wish to buy 2 VIP tickets.`
            )
        cy
            .get("#agree").check()
        cy
            .get("#signature").type(fullName)
        cy
            .get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled")
        cy
            .get("button[type='reset']").click()
        cy
            .get("@submitButton").should("be.disabled")
    })
    it("fills mandatory fields using support command", () =>{
        const customer = {
            firstName: "João",
            lastName: "Silva",
            email: "joaosilva@example.com"
        }
        cy.fillMandatoryFields(customer)

        cy
            .get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled")
        cy
            .get("#agree").uncheck()
        cy
            .get("@submitButton").should("be.disabled")
    })
})